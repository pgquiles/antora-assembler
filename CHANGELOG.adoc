= Antora Assembler Changelog
:url-repo: https://gitlab.com/antora/antora-assembler

This document provides a summary of all notable changes to Antora Assembler by release.
For a detailed view of what's changed, refer to the {url-repo}/commits[commit history] of this project.

== 1.0.0-alpha.9 (2024-04-30)

=== Added

* allow location of config file to be specified using `config_file` key in extension configuration (#48)

=== Fixed

* unconvert page titles from HTML to AsciiDoc to use in aggregate document (#41)
* prevent prepare outlines routine from creating empty items array at top level (#47)
* fix crash when resolving attachment reference in nav when content catalog contains non-publishable files
* don't apply double file extension to assembled PDF when `html_url_extension_style` is indexify (#50)
* require Assembler globally if it cannot be found relative to Antora installation (#49)

=== Changed

* run the `assembleContent` function (to generates the PDFs) during the `navigationBuilt` event instead of the `beforePublish` event (#46)

== 1.0.0-alpha.8 (2024-03-16)

=== Added

* resolve resource reference in value of -image attributes (#13)
* add `build.mkdirs` key to force output directory to be created eagerly before command is run (#34)
* set `aggregate` property to `true` on aggregate document
* set `loader-assembler` attribute by default (#15)

=== Fixed

* escape formatted page title in assembled AsciiDoc (#18)
* use plain page title when comparing to component version title (#18)
* escape all closing square brackets in text of external link, not just the first one
* don't allow `underscore` attribute to be reset if set by page (make it immutable)
* honor symbolic latest version when creating virtual file for aggregate document (#5)
* add default text to link that points to page outside of the aggregate document
* scope IDs for pages not from current component (#37)
* don't insert start page if it points outside current component version (#44)
* convert reference to attachment in nav to external link if site URL is specified (#45)

=== Changed

* prefix default command in pdf-extension with `bundle exec` if Gemfile.lock is found adjacent to the playbook file (#33)
* pass runCommand function as third argument to converter callback function (both as a convenience and to enable unit testing)
* use GeneratorContext#require to require `@antora/assembler` from PDF extension
* skip duplicate internal nav entry if first child of matching nav entry (#36)
* unlink duplicate internal nav entry elsewhere in nav (#36)
* use `keepSource` option on site AsciiDoc config to get document converter to preserve AsciiDoc source (#38)

== 1.0.0-alpha.7 (2023-11-13)

=== Fixed

* replace . with - in generated IDs (#10)
* skip resource reference if resource is not published (e.g., don't crash if target of xref is unpublished page) (#24)
* do not rewrite resource references on commented lines (#31)
* assign version to `revnumber` attribute instead of `author` attribute (#30)
* honor `root_level` value of `1` when component version only has a single navigation menu without a title (#32)
* use `out.path` to reference image in build dir instead of modified `pub.url` value
* mask underscore in path of inline resolved resource (image or attachment) using `underscore` attribute reference (#11)

=== Changed

* set `revdate` using local date instead of date at UTC
* make `revdate` attribute configurable (#29)

== 1.0.0-alpha.6 (2022-09-23)

=== Changed

* log warning message if include directive to reduce cannot be found

=== Fixed

* reify leveloffset to handle cases when leveloffset is set outside of document header
* properly reduce include directives that use the `leveloffset` attribute (#9)

== 1.0.0-alpha.5 (2022-07-24)

=== Fixed

* change default value for `component_versions` key to `*` as documented
* process document title and section titles inside AsciiDoc table cell correctly
* only use source-highlighter specified in Assembler config when converting assembled document
* always ensure that doctype attribute is set in assembler AsciiDoc config
* only set doctype attribute on aggregate document

== 1.0.0-alpha.4 (2022-07-18)

=== Fixed

* show command failed message if EPIPE is thrown (#8)
* fix rewriting of internal xref macro targets
* retain xref macro for internal ref if text part contains attributes

== 1.0.0-alpha.3 (2022-07-08)

=== Fixed

* process block image macro on first line of AsciiDoc table cell
* process multiple image macros on the same line inside a table

== 1.0.0-alpha.2 (2022-07-02)

=== Fixed

* Enable sourcemap even when Asciidoctor extensions are registered
* Unset source-highlighter attribute when producing aggregate document to avoid warnings

== 1.0.0-alpha.1 (2022-05-25)

_Initial prerelease of the software._

////
=== Added
=== Changed
=== Fixed
=== Removed
////
