= Build Keys
:navtitle: Build

The `build` key and its subkeys specify the command to run Asciidoctor PDF and whether the PDFs are published with the HTML site, as well as the build directory, process limit, aggregate AsciiDoc source files.

[#build-key]
== build key

The `build` key is an optional key that can be set in [.path]_antora-assembler.yml_.
It accepts the following key-value pairs:

`command`:: The `command` key is assigned a shell command that runs Asciidoctor PDF and converts the source documents compiled by Assembler to PDFs.
`clean`:: The `clean` key controls whether the directory specified by the `dir` key should be removed before Assembler runs.
`dir`:: The `dir` key specifies the transient directory Assembler uses to prepare and build the PDFs.
`mkdirs`:: The `mkdirs` key specifies whether the output directory of the PDF should be created eagerly, or whether it should be left up to the command.
`keep_aggregate_source`:: The `keep_aggregate_source` indicates whether the aggregate AsciiDoc source files and referenced images under the build directory should be kept for debugging purposes.
`process_limit`:: The `process_limit` key sets the number of concurrent processes that Antora should not exceed when converting the compiled AsciiDoc documents to PDFs.
`publish`:: The `publish` key indicates whether to publish the PDF files to the output destination of the site alongside the HTML files.

[#command-key]
== command key

The `command` key accepts a shell command that runs Asciidoctor PDF and converts the source documents compiled by Assembler to PDFs.
The command runs in the context of the environment in which Antora was launched.
The default value of `command` is `asciidoctor-pdf` (prefixed with `bundle exec` if [.path]_Gemfile.lock_ is detected).
If you are using Bundler to manage the Asciidoctor PDF gem, and you want to customize the command, make sure you prefix the command with `bundle exec` as well.

.antora-assembler.yml
[,yaml]
----
component_versions: '{colorado,1.5.6@colorado}'
asciidoc:
  attributes:
    pdf-theme: ./pdf-theme.yml
build:
  command: bundle exec asciidoctor-pdf
----

Specifying the `command` key gives you an opportunity to pass CLI options to the command.
If you want Asciidoctor PDF to print the full backtrace of an error for debugging purposes, add the `--trace` option.
If you want Asciidoctor PDF to report the file and line number for warnings (when available), add the `--sourcemap` option (Asciidoctor PDF >= 2.2.0).

.antora-assembler.yml
[,yaml]
----
component_versions: '**'
asciidoc:
  attributes:
    source-highlighter: rouge
    pdf-theme: ./pdf-theme.yml
build:
  command: bundle exec asciidoctor-pdf --trace --sourcemap
----

Run `bundle exec asciidoctor-pdf -h` to get a list of available options.
Note that some options are not applicable in this environment (such as `-D`).

[#clean-key]
== clean key

The `clean` key controls whether the directory specified by the `dir` key should be removed before Assembler runs.
By default, this key is assigned the value of the `output.clean` key in the playbook, if specified.
Otherwise, it is assigned the value `false`.

.antora-assembler.yml
[,yaml]
----
build:
  clean: false
----

This key should only be set when using multiple extension instances since leaving behind generated and staged files from a previous run can lead to unexpected and non-reproducible behavior.

[#dir-key]
== dir key

The `dir` key specifies the directory Assembler uses to prepare and build the PDFs.
By default, the build directory is `./build/assembler`

The `dir` key accepts a directory path and uses the same path resolution rules as the output directory in the Antora playbook.

.antora-assembler.yml
[,yaml]
----
component_versions: '{colorado,1.5.6@colorado}'
asciidoc:
  attributes:
    pdf-theme: ./pdf-theme.yml
build:
  command: bundle exec asciidoctor-pdf
  dir: ./build/export
----

[#mkdirs-key]
== mkdirs key

The `mkdirs` key tells Assembler to create the directory where the command specified in the `command` key is going to write the output file.
By default, this key is assigned the value `false`.
To have Assembler create the output directory and any intermediatry directories leading up to it, assign the value `true` to `mkdirs` key in [.path]_antora-assembler.yml_.

.antora-assembler.yml
[,yaml]
----
build:
  mkdirs: true
----

This key should be set to `true` if the command is not capable of creating the directory for the output file itself.

[#keep-aggregate-source-key]
== keep_aggregate_source key

The `keep_aggregate_source` key indicates whether the aggregate AsciiDoc source files and referenced images under the build directory should be kept.
By default, this key is assigned the value `false` and the aggregated AsciiDoc source file for each PDF compiled by Assembler are removed once it completes its operations.
To keep these intermediate source files, which may be helpful when debugging certain issues, assign the value `true` to `keep_aggregate_source` in [.path]_antora-assembler.yml_.

.antora-assembler.yml
[,yaml]
----
component_versions: '{colorado,1.5.6@colorado}'
asciidoc:
  attributes:
    pdf-theme: ./pdf-theme.yml
build:
  command: bundle exec asciidoctor-pdf
  dir: ./build/export
  keep_aggregate_source: true
----

In the above example, the aggregate source files and referenced images will be located under path assigned to the `dir` key.
If you're using the default build directory (`./build/assembler`), they'll be located under that directory.

[#process-limit-key]
== process_limit key

The `process_limit` key sets the number of concurrent processes that Antora should not exceed when converting the compiled AsciiDoc documents to PDFs.
It accepts a number as a value.

.antora-assembler.yml
[,yaml]
----
component_versions: '{colorado,1.5.6@colorado}'
asciidoc:
  attributes:
    pdf-theme: ./pdf-theme.yml
build:
  command: bundle exec asciidoctor-pdf
  process_limit: 2
----

The default value is `auto`, which resolves to half the number of available CPUs.
Use a falsy value (e.g., `false`, `0`, or `~`) to remove the limit.

[#publish-key]
== publish key

The `publish` key indicates whether to publish the PDF files to the output destination of the site alongside the HTML files.
This key accepts the values `true` or `false`.
The default value is `true`, which means the PDF files are published to the site's output destination(s).

If you want the PDF extension to generate the PDFs into the build directory, but not publish them to the site's output destination(s), set the value of the `publish` key to `false` in your [.path]_antora-assembler.yml_ file:

.antora-assembler.yml
[,yaml]
----
component_versions: '{colorado,1.5.6@colorado}'
asciidoc:
  attributes:
    pdf-theme: ./pdf-theme.yml
build:
  command: bundle exec asciidoctor-pdf
  publish: false
----

NOTE: If `publish` is assigned the value `false`, the PDF files are still generated and written to the build directory, but they will not be published to the site.

If you don't want Assembler to generate the PDFs, then set the command to `'true'` (including the quotes):

.antora-assembler.yml
[,yaml]
----
build:
  command: 'true'
  keep_aggregate_source: true
----

TIP: On *nix, you can set the command to `/bin/true` instead.

You might want to do this if you are only interested in creating the aggregate (AsciiDoc) source.

An alternative to using the command `true` is to create an empty shell script and reference it in the `command` key.
