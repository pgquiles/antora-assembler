= PDF Configuration Keys
:description: The Antora PDF extension provides keys for configuring the PDF structure and size, PDF-specific AsciiDoc attributes, and the PDF extension build process.

The table below lists the keys that can be defined in [.path]_antora-assembler.yml_.

WARNING: This is alpha software!
This software is experimental, and it's likely that some key names, their values, or their behavior will change before the final stable version of the PDF extension is released.
You're free to test the keys and give feedback in the {url-chat}[project chat], but take caution when relying on them in a production site.

[cols="2,6,2,2"]
|===
|Name |Description |Default |Values

|xref:component-versions.adoc[component_versions]
|A filter that specifies which component versions the PDF extension exports to PDFs.
|`*`
|Array of picomatch patterns

|xref:root-level.adoc[root_level]
a|Specifies the navigation list entry level from which to start making PDFs.

* `0` makes one PDF per component version.
* `1` makes a PDF per top-level entry in the navigation tree of each component version.
In this case, top-level means each navigation list.
However, if there's only a single navigation list with no title, a PDF will be created for each first-level item in that list.

|`0`
|`0` or `1`

|xref:section-merge-strategy.adoc[section_merge_strategy]
|Controls how the PDF extension merges page sections with sections created to represent navigation entries.
|`discrete`
|`discrete`, `enclose`, or `fuse`

|xref:asciidoc-attributes.adoc[asciidoc.attributes]
|AsciiDoc document attributes that are applied to each compiled AsciiDoc document when exporting to PDF.
These attributes supplement attributes defined in the playbook, component version descriptors, and pages.
If the `doctype` is not specified, it defaults to `book`.
If the `revdate` is not specified, it defaults to the local date.
The `page-partial` is unset.
The `loader-assembler` attribute is set for use in preprocessor conditionals.
|`doctype: book`
|Map of built-in and custom AsciiDoc attributes for PDF generation.

|xref:build.adoc#command-key[build.command]
|The command to run to convert an AsciiDoc document to PDF.
|`asciidoctor-pdf` (prefixed with `bundle exec` if [.path]_Gemfile.lock_ is detected)
|A shell command that runs in the context of the environment in which Antora was launched.

|xref:build.adoc#clean-key[build.clean]
|Whether the directory specified by the `dir` key should be removed before Assembler runs.
|The value of the `output.clean` key in the playbook, if specified, otherwise `false`
|Boolean

|xref:build.adoc#dir-key[build.dir]
|The transient directory Assembler uses to prepare and build the PDFs.
|`./build/assembler`
|A directory path.
Uses the same path resolution rules as the output dir in the Antora playbook.

|xref:build.adoc#keep-aggregate-source-key[build.keep_aggregate_source]
|Whether to keep the aggregate AsciiDoc source files and referenced images under the build dir.
|`false`
|Boolean

|xref:build.adoc#process-limit-key[build.process_limit]
|The number of concurrent processes that Antora should not exceed when converting the compiled AsciiDoc documents to PDFs.
|_auto_ (half the number of CPUs)
|Number

|xref:build.adoc#publish-key[build.publish]
|Whether to publish the PDF files to the output destination of the site alongside the HTML files.
|`true`
|Boolean
|===
