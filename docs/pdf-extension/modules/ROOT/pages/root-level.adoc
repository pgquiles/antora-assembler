= Root Level Key
:navtitle: Root Level
:description: The root_level key controls at what navigation tree entry level a PDF is started.

The `root_level` key controls at what navigation tree entry level a PDF is started.

[#root-level-key]
== root_level key

The `root_level` key is an optional key that can be set in [.path]_antora-assembler.yml_.
It accepts the value `0` or `1`.

`0`:: _Default_.
The value `0` makes one PDF per specified component version, starting from the default start page (_index.adoc_ in the ROOT module) or the page assigned to the `start_page` key in [.path]_antora.yml_.
`1`:: The value `1` makes a PDF starting from each top-level entry in the navigation tree of each specified component version.

If `root_level` isn't set, the extension will automatically set and assign it the value `0` at runtime and generate a PDF per specified component version.

== Generate a PDF per top-level navigation entry

The Antora PDF extension generates a PDF starting from each top-level navigation entry in a component version's navigation tree when the `root_level` key is assigned the value `1`.
In [.path]_antora-assembler.yml_, enter the key name `root_level` directly followed by a colon (`:`), a space, and then the value `1`.

.antora-assembler.yml
[,yaml]
----
component_versions: '{colorado,5.2@colorado}'
root_level: 1 # <.>
----
<.> This is one of the rare cases where you don't enclose a numerical value in quotes.



