= Section Merge Strategy Key
:navtitle: Section Merge Strategy
:description: The section_merge_strategy key controls how the Antora PDF extension merges page sections with sections created to represent navigation entries.

The `section_merge_strategy` key controls how the extension merges the sections from pages with sections created to represent navigation entries.

[#section-merge-strategy-key]
== section_merge_strategy key

The `section_merge_strategy` key is an optional key that can be set in [.path]_antora-assembler.yml_.
It accepts the following built-in values:

<<discrete,discrete>>:: _Default_.
The `discrete` value converts all section titles (level 1 (`==`) and greater) to discrete headings.
Discrete headings don't appear in a TOC or PDF outline
Only the titles of pages referenced in the navigation files are displayed in the PDF outline.
<<enclose,enclose>>:: The `enclose` value inserts a new section title before the preamble of top-level navigation entries and inserts a child navigation entry for the new section under the top-level page.
The new navigation entry, which encloses the top-level section titles from the page, is inserted before the existing children of the navigation entry, effectively becoming a sibling.
<<fuse,fuse>>:: The `fuse` value inserts sections from the page as children of the navigation entry for the current page.
The top-level section titles in the page are inserted before the existing children of the navigation entry, effectively becoming siblings.

[#discrete]
== discrete value

If `section_merge_strategy` isn't set, the extension sets and assigns it the value `discrete` at runtime.
You can also assign it directly to the `section_merge_strategy` in [.path]_antora-assembler.yml_.

.antora-assembler.yml
[,yaml]
----
section_merge_strategy: discrete
----

Let's examine how the pages of the _colorado 5.2_ component version are assembled into a PDF when `section_merge_strategy` is `discrete`.
As shown in <<ex-cvd-colorado-5-2>>, two navigation files are registered for _colorado 5.2_.

.antora.yml for colorado 5.2
[source#ex-cvd-colorado-5-2,yaml]
----
name: colorado
title: Colorado
version: '5.2'
nav:
- modules/ROOT/nav.adoc <.>
- modules/la-garita/nav.adoc <.>
----
<.> Registered navigation file for the _ROOT_ module.
<.> Registered navigation file for the _la-garita_ module.

The navigation file for the _ROOT_ module contains one entry.

.modules/ROOT/nav.adoc for colorado 5.2
[source#ex-colorado-5-2-root-nav,asciidoc]
----
* xref:ranges.adoc[] <.>
----
<.> A top-level entry referencing a page.

The navigation file for the _la-garita_ module contains two entries.

.modules/la-garita/nav.adoc for colorado 5.2
[source#ex-colorado-5-2-module-nav,asciidoc]
----
* xref:index.adoc[] <.>
** xref:willow-creek.adoc[] <.>
----
<.> A top-level entry.
<.> A child entry of the previous top-level entry.

Now, let's look at the PDF produced for _colorado 5.2_ when `section_merge_strategy` is `discrete`.

image::colorado-5-2-index-discrete.png[PDF for colorado 5.2 when section_merge_strategy is discrete]

The first page in the PDF is the title page.
The title of the component, _Colorado_, is inserted as the title of the PDF (level 0 section title).
The next page of the PDF is the start page of the component version.
For _colorado 5.2_, the start page defaults to _index.adoc_ in the _ROOT_ module.
Because the start page of the component version isn't listed in a navigation file, it's treated as a preface, and therefore, Asciidoctor PDF removes its page title by default.

The _ranges.adoc_ page, the sole and top-level entry in the <<ex-colorado-5-2-root-nav,_ROOT_ module's navigation list>>, is the next page in the PDF.
The title of the page is displayed in the PDF outline because the page is referenced in the navigation file.
However, none of the page's section titles, _Primary ranges_, etc., are listed in the PDF outline because the extension converted them to discrete headings.

image::colorado-5-2-ranges-discrete.png[PDF for colorado 5.2 when section_merge_strategy is discrete]

The _willow-creek.adoc_ page is appended to the _index.adoc_ page of the _la-garita_ module because it's a child entry to the top-level entry for _index.adoc_ in the <<ex-colorado-5-2-module-nav,_la-garita_ navigation file>>.
The titles of these two pages are displayed in the PDF outline because they're entries in a navigation file.

image::colorado-5-2-la-garita-discrete.png[PDF for colorado 5.2 when section_merge_strategy is discrete]

[#enclose]
== enclose value

The `enclose` value can be assigned to the `section_merge_strategy` key in the [.path]_antora-assembler.yml_ file.

.antora-assembler.yml
[,yaml]
----
section_merge_strategy: enclose
----

When a top-level navigation entry references a page that contains a preamble, and `enclose` is assigned to `section_merge_strategy`, the following actions occur:

* The section title _Overview_ (or value assigned to the <<overview-title,overview-title AsciiDoc attribute>>) is inserted between the page's title and the preamble
* Existing section titles on the top-level page become children of the new _Overview_ section
* An entry for the _Overview_ section is inserted directly beneath the top-level entry as a child entry
* Existing child entries of the top-level navigation entry become siblings of the new _Overview_ entry
* Entries referencing the section titles of a child page are inserted as children of the navigation entry for the child page;
section title entries are inserted before any existing children of the navigation entry, effectively becoming siblings

IMPORTANT: Whether the section title entries added to the navigation are displayed in the PDF outline or TOC of the generated PDF depends on the level Assembler assigns to a section while creating the aggregate source document and the {url-pdf-docs}/toc/[toclevels^] and {url-pdf-docs}/pdf-outline/[outlinelevels^] values.

Let's use the files in the _la-garita_ module from the _colorado 6.0_ component version to demonstrate how `enclose` will impact the resulting PDF.
The navigation file for the _la-garita_ module lists two files.

.modules/la-garita/nav.adoc for colorado 6.0
[source#ex-child-nav,asciidoc]
----
* xref:index.adoc[] <.>
** xref:willow-creek.adoc[] <.>
----
<.> A top level entry.
<.> A child entry of the previous top level entry.

The source content of the _la-garita_ module's _index.adoc_ file contains a page title, preamble, and level 1, 2, and 3 section titles.

.modules/la-garita/pages/index.adoc for colorado 6.0
[source#ex-top,asciidoc]
----
include::example$index.adoc[]
----

The source content of the _willow-creek_ file also contains a page title, preamble, and level 1 section title.

.modules/la-garita/pages/willow-creek.adoc for colorado 6.0
[source#ex-child,asciidoc]
----
include::example$willow-creek.adoc[]
----

Now, let's look at these pages in the PDF produced for _colorado 6.0_.

image::colorado-6-0-enclose.png[PDF for colorado 6.0 when section_merge_strategy is enclose]

Notice that the section title _Overview_ is inserted above the preamble in the _index.adoc_ page.
This title is also displayed in the PDF outline.
However, because the _willow-creek.adoc_ page is a child entry in the <<ex-child-nav,navigation file>>, an _Overview_ section isn't inserted between the page title and its preamble content.

If you activate the xref:build.adoc#keep-aggregate-source-key[build.keep_aggregate_source] key and review the AsciiDoc source file Assembler creates for the PDF extension, you can see how Assembler inserts the _Overview_ section and automatically recalculates the levels of the subsequent child and sibling section titles.
<<ex-aggregate>> shows a snippet of the aggregate source produced when `section_merge_strategy` is `enclose`.

.Snippet of aggregate source file for colorado 5.2
[source#ex-aggregate,asciidoc]
----
// ...
[#la-garita:index:::]
== La Garita Mountains

:!sectids:
=== Overview
:sectids:

The La Garita Mountains are a subrange in the San Juan Mountains.

[#la-garita:index:::wilderness]
==== La Garita Wilderness

The La Garita Wilderness is located in the La Garita Mountains.
It overlaps areas of the Gunnison National Forest and Rio Grande National Forest.

[#la-garita:index:::trails]
===== Trails

The wilderness area contains numerous backpacking trails as well as shorter trails, such as the <<la-garita:willow-creek:::,East Willow Creek trail>>.

// ...

[#la-garita:willow-creek:::]
=== Willow Creek

Willow Creek runs through the town of Creede, CO.
----

[#overview-title]
=== overview-title attribute

When `enclose` is assigned to the `section_merge_strategy` key, the inserted section titles and their navigation entries are displayed as _Overview_ by default.
This title can be changed with the `overview-title` AsciiDoc attribute.
In [.path]_antora-assembler.yml_, under the `asciidoc` and `attributes` keys, set `overview-title` and assign it your preferred title.

.antora-assembler.yml
[,yaml]
----
section_merge_strategy: enclose
asciidoc:
  attributes:
    overview-title: Introduction
----

[#fuse]
== fuse value

The `fuse` value can be assigned to the `section_merge_strategy` key in the [.path]_antora-assembler.yml_ file.

.antora-assembler.yml
[,yaml]
----
section_merge_strategy: fuse
----

The `fuse` value inserts entries referencing the section titles of a page as children of the navigation entry for the page.
The entries are inserted before any existing children of the navigation entry, effectively becoming siblings.
Whether the section title entries added to the navigation are displayed in the PDF outline or TOC of the generated PDF depends on the level Assembler assigns to a section while creating the aggregate source document and the {url-pdf-docs}/toc/[toclevels^] and {url-pdf-docs}/pdf-outline/[outlinelevels^] values.

Let's use the navigation file shown in <<ex-pre-fuse-nav>> for the _la-garita_ module from the _colorado 6.0_ component version to demonstrate how `fuse` will impact the resulting PDF.
The navigation file for the _la-garita_ module lists two files.
The entry for the _willow-creek.adoc_ file is a child of the _index.adoc_ entry.

.modules/la-garita/nav.adoc for colorado 6.0
[source#ex-pre-fuse-nav,asciidoc]
----
* xref:index.adoc[]
** xref:willow-creek.adoc[]
----

The _index.adoc_ source content in <<ex-fuse-top>> contains a page title and level 1, 2, and 3 section titles.

.modules/la-garita/pages/index.adoc for colorado 6.0
[source#ex-fuse-top,asciidoc]
----
include::example$index.adoc[]
----

Let's look at the resulting PDF outline when `section_merge_strategy` is `fuse` using the navigation file in <<ex-pre-fuse-nav>> and _index.adoc_ and _willow-creek.adoc_ files as inputs.

image::colorado-6-0-fuse.png[PDF for colorado 6.0 when section_merge_strategy is fuse]

The section title _La Garita Wilderness_ from _index.adoc_ is inserted as a child entry into the navigation, and therefore the PDF outline and TOC (if activated).
The child navigation entry for the _willow-creek.adoc_ page becomes a sibling of the _La Garita Wilderness_ entry.