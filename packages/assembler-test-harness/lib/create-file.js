'use strict'

const File = require('vinyl')

// FIXME this needs to be much more comprehensive
function createFile (src, rel, versionSegment = src.version, htmlUrlExtensionStyle) {
  const family = src.family
  const familySegment = family === 'alias' ? 'aliases' : family + 's'
  const relative = src.relative
  const path = `modules/${src.module}/${familySegment}/${relative}`
  if (family === 'partial' || ~relative.split('/').findIndex((it) => it.startsWith('_'))) {
    return new File({ path, contents: src.contents, src })
  }
  let moduleRootPath = Array(relative.split('/').length - 1)
    .fill('..')
    .join('/')
  let outPath = [
    src.component === 'ROOT' ? '' : src.component,
    versionSegment,
    src.module === 'ROOT' ? '' : src.module,
    family === 'page' ? '' : '_' + familySegment,
    family === 'page' ? relative.replace(/\.adoc$/, '') : relative,
  ]
    .filter((it) => it)
    .join('/')
  if (family === 'page') {
    if (htmlUrlExtensionStyle === 'indexify') {
      outPath += '/index.html'
      moduleRootPath += '/..'
    } else {
      outPath += '.html'
    }
  }
  return new File({
    path,
    contents: src.contents,
    src,
    out: { path: outPath },
    pub: { url: '/' + outPath, moduleRootPath },
    ...(rel && { rel }),
  })
}

module.exports = createFile
