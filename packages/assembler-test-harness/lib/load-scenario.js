'use strict'

const { loadAsciiDoc, resolveAsciiDocConfig } = require('@antora/asciidoc-loader')
const loadAssemblerConfig = require('@antora/assembler/load-config')
const fsp = require('node:fs/promises')
const yaml = require('js-yaml')
const ospath = require('node:path')
const { posix: path } = ospath
const createFile = require('./create-file')

const $components = Symbol('components')
const $files = Symbol('files')

class ContentCatalog {
  constructor (components, files, latestVersionSegment, htmlUrlExtensionStyle) {
    this[$components] = components
    this[$files] = files
    this.latestVersionSegment = latestVersionSegment
    this.htmlUrlExtensionStyle = htmlUrlExtensionStyle
  }

  getComponent (name) {
    return this[$components].find((it) => it.name === name)
  }

  getComponents () {
    return [...this[$components]]
  }

  getComponentVersion (name, version) {
    return this.getComponent(name)?.versions.find((it) => it.version === version)
  }

  getById ({ component, version, module: module_, family, relative }) {
    return this[$files].find(({ src }) => {
      return (
        src.component === component &&
        src.version === version &&
        src.module === module_ &&
        src.family === family &&
        src.relative === relative
      )
    })
  }

  getByPath ({ component, version, path: path_ }) {
    return this[$files].find(({ src }) => {
      return src.component === component && src.version === version && src.path === path_
    })
  }

  getComponentVersionStartPage (component, version) {
    return (
      this.getById({ component, version, module: 'ROOT', family: 'page', relative: 'index.adoc' }) ||
      (this.getById({ component, version, module: 'ROOT', family: 'alias', relative: 'index.adoc' }) || {}).rel
    )
  }

  resolvePage (spec, ctx = {}) {
    const src = parseResourceSpec(spec, ctx)
    if (src.component !== ctx.component) src.version ??= this.getComponent(src.component).latest.version
    return (
      this.getById(Object.assign(src, { family: 'page' })) ||
      (this.getById(Object.assign(src, { family: 'alias' })) || {}).rel
    )
  }

  resolveResource (spec, ctx = {}, defaultFamily = undefined) {
    const src = parseResourceSpec(spec, ctx)
    if (src.component !== ctx.component) src.version ??= this.getComponent(src.component).latest.version
    src.family ??= defaultFamily || 'page'
    return this.getById(src)
  }

  getFiles () {
    return this[$files]
  }

  getPages (filter) {
    return this[$files].filter(
      (candidate) => candidate.src.family === 'page' && (filter ? filter(candidate) : candidate)
    )
  }

  // TODO support ctx (i.e., componentVersion) argument
  addFile ({ src, rel }) {
    const versionSegment =
      src.version && this.getComponent(src.component).latest.version === src.version
        ? this.latestVersionSegment
        : undefined
    const file = createFile(src, rel, versionSegment, this.htmlUrlExtensionStyle)
    this[$files].push(file)
    return file
  }

  removeFile (file) {
    this[$files] = this[$files].filter((it) => it !== file)
  }
}

async function loadScenario (name, dirname) {
  const dir = ospath.join(dirname, 'scenarios', name)
  let data = await fsp.readFile(ospath.join(dir, 'data.yml')).then((contents) => yaml.load(contents))
  if (!data.version) data.version = ''
  data = Object.assign(
    {
      displayVersion: data.version || 'default',
      title: toTitle(data.name),
      origin: {
        url: `https://github.com/acme/${data.name}`,
        startPath: '',
        branch: data.version ? `v${data.version}` : 'main',
        refhash: 'a00000000000000000000000000000000000000z',
      },
    },
    data
  )
  const asciidocConfig = resolveAsciiDocConfig()
  const asciidocConfigForInlineOnly = Object.assign({ doctype: 'inline' }, asciidocConfig)
  // TODO should use collateAsciiDocAttributes here to handle overrides properly
  Object.assign(asciidocConfig.attributes, data.asciidoc?.attributes)
  const latestVersionSegment = data.latestVersionSegment
  const htmlUrlExtensionStyle = data.htmlUrlExtensionStyle
  const versionSegment = data.version && (latestVersionSegment == null ? data.version : latestVersionSegment)
  const components = []
  const componentVersion = ensureComponentVersion(components, data, asciidocConfig)
  const files = data.files.map((src) => {
    const asciidoc = src.relative.endsWith('.adoc') && (src.family ??= 'page') === 'page' ? {} : undefined
    src = Object.assign(
      {
        component: componentVersion.name,
        version: 'component' in src ? '' : componentVersion.version,
        module: 'ROOT',
      },
      src
    )
    src.path = path.join('modules', src.module, `${src.family}s`, src.relative) // required by getByPath
    const contents = src.contents || ''
    src.contents = Buffer.from(contents)
    if (src.component === componentVersion.name) {
      src.origin = data.origin
    } else {
      ensureComponentVersion(components, { name: src.component, version: src.version }, asciidocConfig)
      src.origin = {
        url: `https://github.com/acme/${src.component}`,
        startPath: '',
        branch: src.version ? `v${src.version}` : 'main',
        refhash: 'b00000000000000000000000000000000000000y',
      }
    }
    ;({ base: src.basename, ext: src.extname } = path.parse(src.relative))
    if (asciidoc) {
      // should we do this more formally?
      if (contents?.startsWith('= ')) {
        let doctitle = contents.split('\n')[0].slice(2)
        if (/[\x60_*#[]/.test(doctitle)) {
          const doctitleFile = { contents: doctitle, src: { relative: 'dummy.adoc' }, pub: { moduleRootPath: '' } }
          doctitle = loadAsciiDoc(doctitleFile, undefined, asciidocConfigForInlineOnly).convert()
        }
        const navtitleIdx = contents.indexOf('\n:navtitle: ')
        const navtitle = ~navtitleIdx ? contents.slice(navtitleIdx + 12).split('\n')[0] : doctitle
        Object.assign(asciidoc, { doctitle, navtitle, xreftext: doctitle })
      } else {
        Object.assign(asciidoc, { doctitle: 'Untitled', navtitle: 'Untitled', xreftext: 'Untitled' })
      }
    }
    const fileVersionSegment =
      src.component === componentVersion.name
        ? versionSegment
        : src.version && (latestVersionSegment == null ? src.version : latestVersionSegment)
    const file = createFile(src, undefined, fileVersionSegment, htmlUrlExtensionStyle)
    if (asciidoc) file.asciidoc = asciidoc
    return file
  })
  const contentCatalog = new ContentCatalog(components, files, latestVersionSegment, htmlUrlExtensionStyle)
  if (data.startPage) {
    const startPage = contentCatalog.resolvePage(data.startPage, {
      component: componentVersion.name,
      version: componentVersion.version,
    })
    // TODO verify alias isn't going to override index page for component version
    if (startPage) {
      const startPageAliasSrc = {
        component: componentVersion.name,
        version: componentVersion.version,
        module: 'ROOT',
        family: 'alias',
        relative: 'index.adoc',
      }
      contentCatalog.addFile({ src: startPageAliasSrc, rel: startPage })
    }
  }
  const assemblerConfig = await loadAssemblerConfig({ dir }, data.assembler)
  const nav = data.navigation
  if (Array.isArray(nav) && !nav.length) {
    componentVersion.navigation = [] // emulate nav file with no contents
  } else {
    // FIXME support multiple navigation menus
    const navigationMenu = expandNavigation(nav, contentCatalog, componentVersion)
    if (!navigationMenu.content) {
      if ('content' in navigationMenu) {
        delete navigationMenu.content
      } else {
        navigationMenu.content = componentVersion.title // convenience
      }
    }
    componentVersion.navigation = [navigationMenu]
  }
  return { loadAsciiDoc, dir, componentVersion, contentCatalog, assemblerConfig }
}

function ensureComponentVersion (components, { name, version, displayVersion, title }, asciidocConfig) {
  const component = components.length ? components.find((it) => it.name === name) : undefined
  let componentVersion
  if (!(componentVersion = component?.versions.find((it) => it.version === version))) {
    displayVersion ??= version || 'default'
    title ??= toTitle(name)
    const url = '/' + [name === 'ROOT' ? '' : name, version, 'index.html'].filter((it) => it).join('/')
    componentVersion = { name, version, displayVersion, title, url, asciidoc: asciidocConfig }
    component
      ? component.versions.push(componentVersion)
      : components.push({ name, versions: [componentVersion], latest: componentVersion })
  }
  return componentVersion
}

function expandNavigation (item, contentCatalog, componentVersion) {
  const { content, items } = item
  if (content?.startsWith('xref:')) {
    const [, target, text] = /^xref:([^[]+)\[(.*?)\]$/.exec(content)
    const ctx = { component: componentVersion.name, version: componentVersion.version }
    const file = contentCatalog.resolveResource(target, ctx, 'page')
    if (file) {
      item.url = file.pub.url
      item.urlType = 'internal'
      item.content = file.src.family === 'page' ? text || file.asciidoc.navtitle : text
    }
  }
  if (items) {
    for (const item of items) expandNavigation(item, contentCatalog, componentVersion)
  }
  return item
}

function parseResourceSpec (spec, { component, version, module: module_ = 'ROOT' }) {
  const parts = spec.split(':')
  const segments = parts.slice(0, 2)
  if (parts.length > 2) segments.push(parts.slice(2).join(':'))
  let family, relative
  if (segments.length === 1) {
    relative = segments[0]
  } else if (segments.length === 2) {
    ;[module_, relative] = segments
  } else {
    if (!segments[1]) segments[1] = 'ROOT'
    ;[component, module_, relative] = segments
    if (~component.indexOf('@')) {
      ;[version, component] = component.split('@')
    } else {
      version = undefined
    }
  }
  if (~relative.indexOf('$')) [family, relative] = relative.split('$')
  return { component, version, module: module_, family, relative }
}

function toTitle (string) {
  return string
    .split('-')
    .map((it) => it.charAt().toUpperCase() + it.slice(1))
    .join(' ')
}

module.exports = loadScenario
