'use strict'

const loadConfig = require('./load-config')
const produceAggregateDocuments = require('./produce-aggregate-documents')
const PromiseQueue = require('./util/promise-queue')
const runCommand = require('./util/run-command')

async function assembleContent (playbook, contentCatalog, convertDocument, { siteCatalog, configSource }) {
  // Q: could we get ContentCatalog#getComponentVersionStartPage() in Antora core?
  if (typeof contentCatalog.getComponentVersionStartPage !== 'function') {
    contentCatalog.getComponentVersionStartPage = function (component, version) {
      return this.resolvePage('index.adoc', { component, version })
    }
  }
  const assemblerConfig = await loadConfig(playbook, configSource)
  if (!assemblerConfig) return [] // TODO consider removing and doing this another way
  const generatorFunctions = this ? this.getFunctions() : {}
  const { loadAsciiDoc = require('@antora/asciidoc-loader') } = generatorFunctions
  const aggregateDocuments = produceAggregateDocuments(loadAsciiDoc, contentCatalog, assemblerConfig)
  if (!convertDocument) return aggregateDocuments
  const { publishSite: publishFiles = require('@antora/site-publisher') } = generatorFunctions
  const buildConfig = assemblerConfig.build
  await prepareWorkspace(publishFiles, aggregateDocuments, contentCatalog, buildConfig)
  // TODO: pass more information to convertDocument so it doesn't have to compute internal stuff
  // Q: don't we need to pass in the combined/resolved AsciiDoc attributes per file or component version?
  return new PromiseQueue({ concurrency: buildConfig.processLimit })
    .add(aggregateDocuments.map((doc) => () => convertDocument.call(this, doc, buildConfig, runCommand)))
    .toPromise()
    .then((files) => {
      if (buildConfig.publish && siteCatalog) siteCatalog.addFiles(files)
      return files
    })
}

// TODO: if no workspace dir is defined, we shouldn't continue
function prepareWorkspace (publishFiles, aggregateDocuments, contentCatalog, buildConfig) {
  const { dir, clean, keepAggregateSource } = buildConfig
  const files = contentCatalog.findBy({ family: 'image' }).filter(({ out }) => out?.assembled)
  if (keepAggregateSource) {
    files.push(...aggregateDocuments.map((file) => Object.assign(file, { out: { path: file.path } })))
  }
  // TODO: site publisher should accept a single catalog
  return publishFiles({ output: { clean, dir } }, [{ getFiles: () => files }])
}

module.exports = assembleContent
