/* eslint-env mocha */
'use strict'

const { expect } = require('@antora/assembler-test-harness')
const loadConfig = require('@antora/assembler/load-config')
const ospath = require('node:path')

const FIXTURES_DIR = ospath.join(__dirname, 'fixtures', ospath.basename(__filename, '.js'))

describe('loadConfig()', () => {
  let playbook

  beforeEach(() => {
    playbook = { dir: FIXTURES_DIR }
  })

  it('should load config from default file', async () => {
    const actual = await loadConfig(playbook)
    expect(actual.constructor).to.equal(Object)
    expect(actual.componentVersions).to.eql(['**'])
    expect(actual.rootLevel).to.equal(1)
    expect(actual.insertStartPage).to.be.false()
    expect(actual).to.have.nested.property('asciidoc.attributes')
    expect(actual.asciidoc.attributes).to.include({ sectnums: 'all', toc: '', doctype: 'book', 'loader-assembler': '' })
    expect(actual).to.have.property('build')
    expect(actual.build.keepAggregateSource).to.be.true()
  })

  it('should set revdate AsciiDoc attribute if not specified', async () => {
    const actual = await loadConfig(playbook)
    expect(actual.constructor).to.equal(Object)
    expect(actual).to.have.nested.property('asciidoc.attributes.revdate')
    const dateString = actual.asciidoc.attributes.revdate
    const dateTimestamp = +Date.parse(dateString)
    const now = new Date()
    const nowLocalMs = now - now.getTimezoneOffset() * 60000
    expect(dateTimestamp).to.be.closeTo(nowLocalMs, 24 * 60 * 60000)
  })

  it('should not camelCase names of AsciiDoc attributes when config is read from file', async () => {
    const actual = await loadConfig(playbook)
    expect(actual.constructor).to.equal(Object)
    expect(actual).to.have.nested.property('asciidoc.attributes.source-highlighter', 'rouge')
  })

  it('should allow config file path to be specified', async () => {
    const actual = await loadConfig(playbook, './antora-assembler-config.yml')
    expect(actual.constructor).to.equal(Object)
    expect(actual.componentVersions).to.eql(['*'])
    expect(actual.rootLevel).to.equal(0)
    expect(actual).to.have.nested.property('asciidoc.attributes')
    expect(actual.asciidoc.attributes).to.include({ 'source-highlighter': 'rouge', toc: '' })
    expect(actual).to.have.property('build')
    expect(actual.build.keepAggregateSource).to.be.true()
  })

  it('should return object with default values if default config file is not found', async () => {
    playbook.dir = ospath.dirname(FIXTURES_DIR)
    const actual = await loadConfig(playbook)
    expect(actual.constructor).to.equal(Object)
    expect(actual.insertStartPage).to.be.true()
    expect(actual.rootLevel).to.equal(0)
    expect(actual.sectionMergeStrategy).to.equal('discrete')
    expect(actual.componentVersions).to.eql(['*'])
    expect(actual).to.have.nested.property('asciidoc.attributes')
    expect(actual).to.have.nested.property('asciidoc.attributes.doctype', 'book')
    expect(actual).to.have.property('build')
    expect(actual.build.dir).to.equal(ospath.join(playbook.dir, 'build/assembler'))
    expect(actual.build.cwd).to.equal(playbook.dir)
    expect(actual.build.publish).to.be.true()
    expect(actual.build.processLimit).to.be.greaterThan(0)
  })

  it('should return object with default values if custom config file is not found', async () => {
    const actual = await loadConfig(playbook, './does-not-exist.yml')
    expect(actual.constructor).to.equal(Object)
    expect(actual.insertStartPage).to.be.true()
    expect(actual.rootLevel).to.equal(0)
    expect(actual.sectionMergeStrategy).to.equal('discrete')
    expect(actual.componentVersions).to.eql(['*'])
    expect(actual).to.have.nested.property('asciidoc.attributes')
    expect(actual).to.have.property('build')
    expect(actual.build.dir).to.equal(ospath.join(playbook.dir, 'build/assembler'))
    expect(actual.build.cwd).to.equal(playbook.dir)
    expect(actual.build.publish).to.be.true()
    expect(actual.build.processLimit).to.be.greaterThan(0)
  })

  it('should accept custom config source', async () => {
    const input = { componentVersions: '**', sectionMergeStrategy: 'enclose' }
    const actual = await loadConfig(playbook, input)
    expect(actual.constructor).to.equal(Object)
    expect(actual.sectionMergeStrategy).to.equal('enclose')
    expect(actual.componentVersions).to.eql(['**'])
  })

  it('should not override specified revdate', async () => {
    const input = { asciidoc: { attributes: { revdate: '2020-01-01' } } }
    const actual = await loadConfig(playbook, input)
    expect(actual.constructor).to.equal(Object)
    expect(actual.asciidoc.attributes.revdate).to.equal('2020-01-01')
  })

  it('should not camelCase keys in custom config source', async () => {
    const input = { section_merge_strategy: 'enclose' }
    const actual = await loadConfig(playbook, input)
    expect(actual.constructor).to.equal(Object)
    expect(actual.section_merge_strategy).to.equal('enclose')
    expect(actual.sectionMergeStrategy).to.equal('discrete')
  })

  it('should return undefined if value of enabled key is false', async () => {
    const input = { enabled: false }
    const actual = await loadConfig(playbook, input)
    expect(actual).to.be.undefined()
  })

  it('should coerce value of componentVersions key to array if specified as string', async () => {
    const input = { componentVersions: 'component-a' }
    const actual = await loadConfig(playbook, input)
    expect(actual.constructor).to.equal(Object)
    expect(actual.componentVersions).to.be.instanceOf(Array)
    expect(actual.componentVersions).to.eql(['component-a'])
  })

  it('should coerce value of componentVersions key to array if specified as ventilated comma-separated string', async () => {
    const input = { componentVersions: '*, *@component-a' }
    const actual = await loadConfig(playbook, input)
    expect(actual.constructor).to.equal(Object)
    expect(actual.componentVersions).to.be.instanceOf(Array)
    expect(actual.componentVersions).to.eql(['*', '*@component-a'])
  })

  it('should only split componentVersion string on commas followed by a space', async () => {
    const input = { componentVersions: '{1,2}.*@component-a, component-{b,c}' }
    const actual = await loadConfig(playbook, input)
    expect(actual.constructor).to.equal(Object)
    expect(actual.componentVersions).to.be.instanceOf(Array)
    expect(actual.componentVersions).to.eql(['{1,2}.*@component-a', 'component-{b,c}'])
  })

  it('should ignore invalid value for section_merge_strategy key', async () => {
    const input = { sectionMergeStrategy: 'invalid' }
    const actual = await loadConfig(playbook, input)
    expect(actual.constructor).to.equal(Object)
    expect(actual.sectionMergeStrategy).to.equal('discrete')
  })

  it('should set value of build.clean key if not specified to value of output.clean key in playbook', async () => {
    const input = {}
    playbook.output = { clean: true }
    const actual = await loadConfig(playbook, input)
    expect(actual.constructor).to.equal(Object)
    expect(actual).to.have.nested.property('build.clean', true)
  })

  it('should not override value of build.clean key if specified', async () => {
    const input = { build: { clean: false } }
    playbook.output = { clean: true }
    const actual = await loadConfig(playbook, input)
    expect(actual.constructor).to.equal(Object)
    expect(actual).to.have.nested.property('build.clean', false)
  })

  it('should set value of build.processLimit key to Infinity if value is falsy', async () => {
    const input = { build: { processLimit: null } }
    const actual = await loadConfig(playbook, input)
    expect(actual.constructor).to.equal(Object)
    expect(actual).to.have.nested.property('build.processLimit', Infinity)
  })
})
