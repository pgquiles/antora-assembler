= The Component
:revnumber: 1.0
:doctype: book
:underscore: _
:page-component-name: the-component
:page-component-version: 1.0
:page-version: {page-component-version}
:page-component-display-version: 1.0
:page-component-title: The Component

:docname: the-page
:page-module: ROOT
:page-relative-src-path: the-page.adoc
:page-origin-url: https://github.com/acme/the-component
:page-origin-start-path:
:page-origin-refname: v1.0
:page-origin-reftype: branch
:page-origin-refhash: a00000000000000000000000000000000000000z
[#the-page:::]
== The Page

The macro `\xref:the-page.adoc[]` will link to the current page.

The macro `+xref:other-module:the-page.adoc[]+` will link to the other page.

:docname: the-page
:page-module: other-module
:page-relative-src-path: the-page.adoc
:page-origin-url: https://github.com/acme/the-component
:page-origin-start-path:
:page-origin-refname: v1.0
:page-origin-reftype: branch
:page-origin-refhash: a00000000000000000000000000000000000000z
[#other-module:the-page:::]
== The Other Page

content
