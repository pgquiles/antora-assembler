/* eslint-env mocha */
'use strict'

const { expect } = require('@antora/assembler-test-harness')
const unconvertInlineAsciiDoc = require('#unconvert-inline-asciidoc')

describe('unconvertInlineAsciiDoc()', () => {
  it('should convert empty string', () => {
    const input = ''
    expect(unconvertInlineAsciiDoc(input)).to.be.empty()
  })

  it('should convert plain text', () => {
    const input = 'text'
    expect(unconvertInlineAsciiDoc(input)).to.equal(input)
  })

  it('should escape attribute reference', () => {
    const input = '{name}'
    expect(unconvertInlineAsciiDoc(input)).to.equal('\\' + input)
  })

  it('should escape attribute references in plain text', () => {
    const input = '{foo} and {bar}'
    expect(unconvertInlineAsciiDoc(input)).to.equal('\\{foo} and \\{bar}')
  })

  Object.entries({ code: '`', em: '_', mark: '#', span: '', strong: '*' }).forEach(([tagName, mark]) => {
    it(`should convert isolated ${tagName} tag`, () => {
      const input = `<${tagName}>text</${tagName}>`
      expect(unconvertInlineAsciiDoc(input)).to.equal(`${mark}text${mark}`)
    })

    it(`should convert multiple ${tagName} tags`, () => {
      const input = `<${tagName}>some</${tagName}> <${tagName}>text</${tagName}>`
      expect(unconvertInlineAsciiDoc(input)).to.equal(`${mark}some${mark} ${mark}text${mark}`)
    })

    it(`should convert ${tagName} tag with id`, () => {
      const input = `<${tagName} id="idname">text</${tagName}>`
      if (tagName === 'span') mark = '#'
      expect(unconvertInlineAsciiDoc(input)).to.equal(`[#idname]${mark}text${mark}`)
    })

    it(`should convert ${tagName} tag with single class`, () => {
      const input = `<${tagName} class="rolename">text</${tagName}>`
      if (tagName === 'span') mark = '#'
      expect(unconvertInlineAsciiDoc(input)).to.equal(`[.rolename]${mark}text${mark}`)
    })

    it(`should convert ${tagName} tag with multiple classes`, () => {
      const input = `<${tagName} class="role1 role2">text</${tagName}>`
      if (tagName === 'span') mark = '#'
      expect(unconvertInlineAsciiDoc(input)).to.equal(`[.role1.role2]${mark}text${mark}`)
    })

    it(`should convert ${tagName} tag with id and classes`, () => {
      const input = `<${tagName} id="idname" class="role1 role2">text</${tagName}>`
      if (tagName === 'span') mark = '#'
      expect(unconvertInlineAsciiDoc(input)).to.equal(`[#idname.role1.role2]${mark}text${mark}`)
    })
  })

  it('should convert isolated icon tag', () => {
    const input = '<span class="icon"><i class="fa fa-check-circle"></i></span>'
    expect(unconvertInlineAsciiDoc(input)).to.equal('icon:check-circle[]')
  })

  it('should convert isolated img tag', () => {
    const input = '<span class="image"><img src="name.png" alt="description"></span>'
    expect(unconvertInlineAsciiDoc(input)).to.equal('image:name.png[description]')
  })

  it('should convert img tag adjacent to custom span', () => {
    const input = '<span class="rolename">text</span> <span class="image"><img src="name.png" alt="description"></span>'
    expect(unconvertInlineAsciiDoc(input)).to.equal('[.rolename]#text# image:name.png[description]')
  })

  it('should escape attribute reference in tag', () => {
    const input = '<strong>{name}</strong>'
    expect(unconvertInlineAsciiDoc(input)).to.equal('*\\{name}*')
  })

  it('should convert nested tag', () => {
    const input = '<strong><em>strong with gusto</em></strong>'
    expect(unconvertInlineAsciiDoc(input)).to.equal('*_strong with gusto_*')
  })

  it('should convert deeply nested tag', () => {
    const input = '<strong><mark><em>cannot stress this enough</em></mark></strong>'
    expect(unconvertInlineAsciiDoc(input)).to.equal('*#_cannot stress this enough_#*')
  })

  it('should convert nested tag adjacent to text', () => {
    const input = '<strong>strong <em>with gusto</em></strong>'
    expect(unconvertInlineAsciiDoc(input)).to.equal('*strong _with gusto_*')
  })

  it('should convert img tag inside span', () => {
    const input = '<strong>click <img src="submit.png" alt="Submit"></strong>'
    expect(unconvertInlineAsciiDoc(input)).to.equal('*click image:submit.png[Submit]*')
  })

  it('should convert custom span in custom span', () => {
    const input = '<span class="role1">foo <span class="role2">bar</span> baz</span>'
    expect(unconvertInlineAsciiDoc(input)).to.equal('[.role1]#foo [.role2]##bar## baz#')
  })

  it('should use unconstrained marks when tag preceded by word character', () => {
    const input = 'show<strong>y</strong>'
    expect(unconvertInlineAsciiDoc(input)).to.equal('show**y**')
  })

  it('should use unconstrained marks when tag followed by word character', () => {
    const input = '<strong>in</strong>direct'
    expect(unconvertInlineAsciiDoc(input)).to.equal('**in**direct')
  })

  it('should use unconstrained marks for tag nested in em tag', () => {
    const input = '<em><strong>gusto</strong></em>'
    expect(unconvertInlineAsciiDoc(input)).to.equal('_**gusto**_')
  })
})
