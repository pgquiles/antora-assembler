'use strict'

const fsp = require('node:fs/promises')
const ospath = require('node:path')

// Q: how can we simplify this to avoid redundant code?
// Q: rename to convertDocumentToPDF?
async function convertDocumentToPdf (doc, buildConfig, runCommand) {
  const {
    asciidoc: { attributes: baseAttributes } = { attributes: {} },
    contents: input,
    src: { component, version, basename, extname = doc.extname },
  } = doc
  const { cwd = process.cwd(), dir = cwd } = buildConfig
  const command = buildConfig.command ?? (await scopeDefaultCommand(cwd))
  const docfile = `${version}@${component}::pdf$${basename}`
  const docname = basename.slice(0, basename.length - extname.length)
  const convertAttributes = Object.assign({}, baseAttributes, {
    docfile,
    docfilesuffix: extname,
    'docname@': docname,
    imagesdir: dir,
  })
  Object.assign(doc, { contents: null, extname: '.pdf', mediaType: 'application/pdf' })
  const argv = Object.entries(convertAttributes).reduce(
    (accum, [name, val]) =>
      accum.push('-a', val ? `${name}=${val}` : val === '' ? name : `!${name}${val === false ? '=@' : ''}`) && accum,
    []
  )
  const output = ospath.join(dir, doc.path)
  // Q: should mkdirs be the default behavior?
  if (buildConfig.mkdirs) await fsp.mkdir(ospath.dirname(output), { recursive: true, force: true })
  argv.push('-o', output)
  // Q: should runCommand accept outputFlag and automatically append to argv?
  return runCommand(command, argv, { cwd, input, output }).then((contents) =>
    Object.assign(doc, { contents, out: { path: doc.path } })
  )
}

function scopeDefaultCommand (cwd, baseCommand = 'asciidoctor-pdf') {
  return fsp.stat(ospath.join(cwd, 'Gemfile.lock')).then(
    () => `bundle exec ${baseCommand}`,
    () => baseCommand
  )
}

module.exports = convertDocumentToPdf
