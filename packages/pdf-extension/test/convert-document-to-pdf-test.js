/* eslint-env mocha */
'use strict'

// Q: should extension export this function instead?
const convertDocumentToPdf = require('#convert-document-to-pdf')
const { expect } = require('@antora/assembler-test-harness')
const fsp = require('node:fs/promises')
const os = require('node:os')
const ospath = require('node:path')
const { posix: path } = ospath

const FIXTURES_DIR = ospath.join(__dirname, 'fixtures')

describe('convertDocumentToPdf()', () => {
  const cwd = process.cwd()
  let doc
  let ran

  const dryRunCommand = async (command, argv, opts) => {
    ran = { command, argv, opts }
    return '<pdf stream>'
  }

  beforeEach(() => {
    doc = {
      extname: '.adoc',
      input: Buffer.from('= Assembled'),
      src: { component: 'the-component', version: '1.0', basename: 'assembled.adoc' },
    }
    Object.defineProperty(doc, 'path', {
      get: function () {
        return path.join(this.src.component, this.src.version, this.src.basename.replace(/\.adoc$/, this.extname))
      },
    })
    ran = undefined
  })

  it('should convert media type of doc from AsciiDoc to PDF', async () => {
    const actual = await convertDocumentToPdf(doc, {}, dryRunCommand)
    expect(actual.extname).to.equal('.pdf')
    expect(actual.mediaType).to.equal('application/pdf')
    expect(actual.contents).to.equal('<pdf stream>')
    expect(actual.path).to.match(/\.pdf$/)
    expect(actual.path).to.equal(doc.path)
    expect(actual.out.path).to.equal(doc.path)
  })

  it('should use default command if command not specified', async () => {
    await convertDocumentToPdf(doc, {}, dryRunCommand)
    expect(ran.command).to.equal('asciidoctor-pdf')
  })

  it('should prepend bundle exec to default command if Gemfile.lock is detected at cwd', async () => {
    const cwd_ = ospath.join(FIXTURES_DIR, 'project-with-gemfile')
    await convertDocumentToPdf(doc, { cwd: cwd_ }, dryRunCommand)
    expect(ran.command).to.equal('bundle exec asciidoctor-pdf')
  })

  it('should compute and pass intrinsic attributes to command', async () => {
    await convertDocumentToPdf(doc, {}, dryRunCommand)
    const attributeOptions = {}
    let previousArg
    ran.argv.forEach((arg) => {
      if (previousArg === '-a') {
        const [name, val] = arg.split('=')
        attributeOptions[name] = val
      }
      previousArg = arg
    })
    expect(attributeOptions).to.include({
      docfile: '1.0@the-component::pdf$assembled.adoc',
      docfilesuffix: '.adoc',
      'docname@': 'assembled',
    })
  })

  it('should pass attributes on doc to command', async () => {
    const attributes = { icons: 'font', 'pdf-theme': 'default-with-font-fallbacks' }
    doc.asciidoc = { attributes }
    await convertDocumentToPdf(doc, {}, dryRunCommand)
    const attributeOptions = {}
    let previousArg
    ran.argv.forEach((arg) => {
      if (previousArg === '-a') {
        const [name, val] = arg.split('=')
        attributeOptions[name] = val
      }
      previousArg = arg
    })
    expect(attributeOptions).to.include(attributes)
  })

  it('should pass contents of input document to stdin of command', async () => {
    const input = doc.contents
    await convertDocumentToPdf(doc, {}, dryRunCommand)
    expect(ran.opts.input).to.eql(input)
  })

  it('should set cwd of command to process.cwd() by default', async () => {
    await convertDocumentToPdf(doc, {}, dryRunCommand)
    expect(ran.opts.cwd).to.eql(cwd)
  })

  it('should set cwd of command to cwd specified in buildConfig', async () => {
    const cwd_ = '/path/to/project'
    await convertDocumentToPdf(doc, { cwd: cwd_ }, dryRunCommand)
    expect(ran.opts.cwd).to.eql(cwd_)
  })

  it('should compute output and imagesdir relative to process.cwd() by default', async () => {
    await convertDocumentToPdf(doc, {}, dryRunCommand)
    const dir = cwd
    const expected = ospath.join(dir, doc.path)
    let output
    let imagesdir
    let previousArg
    ran.argv.forEach((arg) => {
      if (previousArg === '-o') {
        output = arg
      } else if (previousArg === '-a' && arg.startsWith('imagesdir=')) {
        imagesdir = arg.slice(10)
      }
      previousArg = arg
    })
    expect(imagesdir).to.equal(dir)
    expect(output).to.equal(expected)
    expect(ran.opts.output).to.equal(expected)
  })

  it('should compute output and imagesdir relative to dir specified in buildConfig', async () => {
    const dir = '/path/to/project/build/assembler'
    await convertDocumentToPdf(doc, { dir }, dryRunCommand)
    const expected = ospath.join(dir, doc.path)
    let imagesdir
    let output
    let previousArg
    ran.argv.forEach((arg) => {
      if (previousArg === '-o') {
        output = arg
      } else if (previousArg === '-a' && arg.startsWith('imagesdir=')) {
        imagesdir = arg.slice(10)
      }
      previousArg = arg
    })
    expect(imagesdir).to.equal(dir)
    expect(output).to.equal(expected)
    expect(ran.opts.output).to.equal(expected)
  })

  it('should not eagerly make output directory by default', async () => {
    const projectDir = await fsp.mkdtemp(ospath.join(os.tmpdir(), 'antora-assembler-'))
    try {
      const dir = ospath.join(projectDir, 'build/assembler')
      await convertDocumentToPdf(doc, { dir }, dryRunCommand)
      const expected = ospath.dirname(ospath.join(dir, doc.path))
      expect(
        await fsp.stat(expected).then(
          (stat) => stat.isDirectory(),
          () => false
        )
      ).to.be.false()
    } finally {
      await fsp.rm(projectDir, { force: true, recursive: true })
    }
  })

  it('should eagerly make output directory if mkdirs option is true', async () => {
    const projectDir = await fsp.mkdtemp(ospath.join(os.tmpdir(), 'antora-assembler-'))
    try {
      const dir = ospath.join(projectDir, 'build/assembler')
      await convertDocumentToPdf(doc, { dir, mkdirs: true }, dryRunCommand)
      const expected = ospath.dirname(ospath.join(dir, doc.path))
      expect(
        await fsp.stat(expected).then(
          (stat) => stat.isDirectory(),
          () => false
        )
      ).to.be.true()
    } finally {
      await fsp.rm(projectDir, { force: true, recursive: true })
    }
  })
})
