/* eslint-env mocha */
'use strict'

const { expect } = require('@antora/assembler-test-harness')

describe('pdf-extension', () => {
  const ext = require('@antora/pdf-extension')

  const createGeneratorContext = () => ({
    once (eventName, fn) {
      this[eventName] = fn
    },
    require,
  })

  let generatorContext, registerVars

  beforeEach(() => {
    generatorContext = createGeneratorContext()
    registerVars = { config: {} }
  })

  describe('bootstrap', () => {
    it('should be able to require extension', () => {
      expect(ext).to.be.instanceOf(Object)
      expect(ext.register).to.be.instanceOf(Function)
    })

    it('should set keepSource on AsciiDoc config during beforeProcess event', () => {
      ext.register.call(generatorContext, registerVars)
      const siteAsciiDocConfig = {}
      generatorContext.beforeProcess({ siteAsciiDocConfig })
      expect(siteAsciiDocConfig.keepSource).to.be.true()
    })

    it('should be able to call register function exported by extension', () => {
      ext.register.call(generatorContext, registerVars)
      expect(generatorContext.navigationBuilt).to.be.instanceOf(Function)
    })

    it('should call assembleContent function when navigationBuilt event is emitted', () => {
      ext.register.call(generatorContext, registerVars)
      const required = []
      let assembleContentArguments
      generatorContext.require = function (id) {
        required.push(id)
        return {
          assembleContent () {
            assembleContentArguments = arguments
          },
        }
      }
      generatorContext.navigationBuilt({})
      expect(required).to.have.lengthOf(1)
      expect(required[0]).to.equal('@antora/assembler')
      expect(assembleContentArguments[2]).to.be.instanceOf(Function)
      expect(assembleContentArguments[3]).to.be.instanceOf(Object)
      expect(assembleContentArguments[3].configSource).to.be.undefined()
    })

    it('should allow configFile to be overridden by extension configuration', () => {
      registerVars.config.configFile = './pdf-config.yml'
      ext.register.call(generatorContext, registerVars)
      let assembleContentArguments
      generatorContext.require = function () {
        return {
          assembleContent () {
            assembleContentArguments = arguments
          },
        }
      }
      generatorContext.navigationBuilt({})
      expect(assembleContentArguments[3]).to.be.instanceOf(Object)
      expect(assembleContentArguments[3].configSource).to.equal('./pdf-config.yml')
    })
  })
})
